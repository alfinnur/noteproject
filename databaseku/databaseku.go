package databaseku

import (
	"database/sql"
	"fmt"
)

type databaseConfig struct {
	Table, Database, Host, ID, Password string
}

func New(table, database, host, id, password string) databaseConfig {
	e := databaseConfig{table, database, host, id, password}
	return e
}

func (e databaseConfig) dbConnect() *sql.DB {
	fmt.Println("CONNECTING")
	open := fmt.Sprintf("%s:%s@tcp(%s)/%s", e.ID, e.Password, e.Host, e.Database)
	db, err := sql.Open("mysql", open) //id+":"+password+"@tcp("+Host+")/"+Database
	if err != nil {
		fmt.Println("LOGIN FAILED")
	}
	fmt.Println("CONNECTED")
	return db
}

func (e databaseConfig) DoQuery(queryText string) (*sql.Rows, error) {
	db := e.dbConnect()
	return db.Query(queryText)
}

func (e databaseConfig) CreateTable(username string) {
	db := e.dbConnect()
	fmt.Println("CreatingTable")
	query := fmt.Sprintf("CREATE TABLE `%s` ( `no` INT NOT NULL AUTO_INCREMENT , `note` TEXT NOT NULL , PRIMARY KEY (`no`));", username)
	selfDB, err := db.Prepare(query)
	if err != nil {
		panic(err.Error())
	}
	if selfDB != nil {
		_, err = selfDB.Exec()
		if err != nil {
			fmt.Println(err.Error())
		}
		defer selfDB.Close()
	}
	fmt.Println("CreatingTable DONE")
}
