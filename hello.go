package main

import (
	"fmt"
	"net/http"
	"noteproject/databaseku"
	"os"
	"strings"
	"text/template"

	_ "github.com/go-sql-driver/mysql"
)

var (
	db             = databaseku.New("akun", "sql12306008", "sql12.freesqldatabase.com:3306", "sql12306008", "SyupQHWrv4")
	currentAccount string //TODO: should use session instead
)

func testShow(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "hello, world yes")
}

func main() {
	// port := "8080" //this for localhost
	port := os.Getenv("PORT")
	http.HandleFunc("/", insertHandler)
	http.HandleFunc("/show", showHandler)
	http.ListenAndServe(":"+port, nil)
}

func insertHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("methodss:", r.Method) //get request method
	if r.Method == http.MethodGet {
		t, _ := template.ParseFiles("html/insert.html")
		myvar := map[string]interface{}{"MyVar": "Foo Bar Baz"}
		t.Execute(w, myvar)
	} else {
		r.ParseForm()
		switch r.FormValue("action") {
		case "register":
			registerAkun(r.PostFormValue("username"), r.PostFormValue("message"), w, r)
		case "login":
			loginAkun(r.PostFormValue("username"), r.PostFormValue("message"), w, r)
		}
	}
}

func showHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		t, _ := template.ParseFiles("html/Show.html")
		datanote := showAllData()
		data := map[string]interface{}{
			"MyVar":    "data",
			"dataNote": datanote,
			"username": currentAccount}
		t.Execute(w, data)
	} else {
		r.ParseForm()
		switch r.FormValue("action") {
		case "Add New":
			err := addNote(r.PostFormValue("message"))
			if err == nil {
				http.Redirect(w, r, "/show", http.StatusFound)
			}
		}
	}
}

func loginAkun(username, message string, w http.ResponseWriter, r *http.Request) {
	query := fmt.Sprintf("SELECT * FROM %s WHERE ID='%s' and PASSWORD='%s'", db.Table, username, message)
	rows, err := db.DoQuery(query)
	if err != nil {
		fmt.Println(err.Error())
	}
	if rows.Next() {
		currentAccount = username
		http.Redirect(w, r, "/show", http.StatusFound)
	}

}

func registerAkun(username, password string, w http.ResponseWriter, r *http.Request) {
	query := fmt.Sprintf("INSERT INTO %s VALUES ( '%s', '%s')", db.Table, username, password)
	_, err := db.DoQuery(query)
	if err != nil {
		if strings.Contains(err.Error(), "1062") {
			fmt.Println("DUPE")
			http.Redirect(w, r, "/", http.StatusFound)
		}
		return
	}
	currentAccount = username
	db.CreateTable(currentAccount)
	http.Redirect(w, r, "/show", http.StatusFound)
}

func showAllData() []string {
	var rowData []string
	query := fmt.Sprintf("SELECT * from %s", currentAccount)
	result, err := db.DoQuery(query)
	if err != nil {
		fmt.Println(err.Error())
		return nil
	}
	for result.Next() {
		var nomer int
		var note string
		result.Scan(&nomer, &note)
		rowData = append(rowData, note)
	}
	return rowData
}

func addNote(text string) error {
	query := fmt.Sprintf("INSERT INTO %s VALUES ( NULL, '%s' )", currentAccount, text)
	_, err := db.DoQuery(query)
	if err != nil {
		fmt.Printf(err.Error())
	}
	return err
}
